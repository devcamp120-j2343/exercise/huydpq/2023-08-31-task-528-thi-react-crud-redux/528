import axios from "axios"
import { ALL_DRINK_AXIOS_ERROR, ALL_DRINK_AXIOS_SUCCESS, CHANGE_INPUT, CREATE_ORDER_ERROR, CREATE_ORDER_SUCCESS, DELETE_ORDER_ERROR, DELETE_ORDER_SUCCESS, GET_ORDER_BY_ID_ERROR, GET_ORDER_BY_ID_SUCCESS, ON_CHANGE_SELECT_SIZE, ON_CHANGE_SELECT_STATUS, ON_CLICK_CLOSE_MODAL, UPDATE_ORDER_ERROR, UPDATE_ORDER_SUCCESS, USER_FETCH_ERROR, USER_FETCH_PENDING, USER_FETCH_SUCCESS } from "../constants/order.constant"
export const fetchURL = () => {
    return async (dispatch) => {
        try {
            await dispatch({
                type: USER_FETCH_PENDING
            })

            let config = {
                method: 'get',
                url: 'http://203.171.20.210:8080/devcamp-pizza365/orders',
                headers: {
                    'Content-Type': 'application/json'
                },
            }
            let reponse = await axios(config)
            let listOrder = reponse.data

            return dispatch({
                type: USER_FETCH_SUCCESS,
                data: listOrder
            })

        } catch (error) {
            return dispatch({
                type: USER_FETCH_ERROR,
                err: error
            })
        }
    }
}
export const createOrder = (order) => {
    return async (dispatch) => {
        try {
            // call API Create
            await axios('http://203.171.20.210:8080/devcamp-pizza365/orders', {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify(order)
            });

            let reponse = await axios({
                method: 'get',
                url: 'http://203.171.20.210:8080/devcamp-pizza365/orders',
                headers: {
                    'Content-Type': 'application/json'
                },
            })
            let listOrder = await reponse.data
            return dispatch({
                type: CREATE_ORDER_SUCCESS,
                warning: true,
                data: listOrder
            })


        } catch (error) {
            return dispatch({
                type: CREATE_ORDER_ERROR,
                warning: false,
                err: error
            })
        }
    }
}
export const getOrderByID = (orderCode) => {
    return async (dispatch) => {
        try {
            // call API Update
            let config = {
                method: 'get',
                url: 'http://203.171.20.210:8080/devcamp-pizza365/orders/' + orderCode,
                headers: {
                    'Content-Type': 'application/json'
                },

            };
            let reponse = await axios(config)
            let data = await reponse.data

            return dispatch({
                type: GET_ORDER_BY_ID_SUCCESS,
                warning: true,
                data: data
            })
        } catch (error) {
            return dispatch({
                type: GET_ORDER_BY_ID_ERROR,
                warning: false,
                err: error
            })
        }
    }
}

export const updateOrder = (order,userId) => {
    return async (dispatch) => {
        try {
            // call API update
            let config = {
                method: 'put',
                url: 'http://203.171.20.210:8080/devcamp-pizza365/orders/' + userId,
                headers: { 
                  'Content-Type': 'application/json'
                },
                data : JSON.stringify(order)
              };

             await axios(config)
            let reponse = await axios({
                method: 'get',
                url: 'http://203.171.20.210:8080/devcamp-pizza365/orders',
                headers: {
                    'Content-Type': 'application/json'
                },
            })
            let data = await reponse.data

            return dispatch({
                type: UPDATE_ORDER_SUCCESS,
                warning: true,
                data: data
            })
        } catch (error) {
            return dispatch({
                type: UPDATE_ORDER_ERROR,
                warning: false,
                err: error
            })
        }
    }
}

export const deteleOrder = (userId) => {
    return async (dispatch) => {
        try {
            // call API Delete
            let config = {
                method: 'delete',
                url: 'http://203.171.20.210:8080/devcamp-pizza365/orders/' + userId,
                headers: { 
                  'Content-Type': 'application/json'
                },
              };

             await axios(config)
            let reponse = await axios({
                method: 'get',
                url: 'http://203.171.20.210:8080/devcamp-pizza365/orders',
                headers: {
                    'Content-Type': 'application/json'
                },
            })
            let data = await reponse.data

            return dispatch({
                type: DELETE_ORDER_SUCCESS,
                warning: true,
                data: data
            })
        } catch (error) {
            return dispatch({
                type: DELETE_ORDER_ERROR,
                warning: false,
                err: error
            })
        }
    }
}

export const changeInput = (values, name) => {
    return {
        type: CHANGE_INPUT,
        value: values,
        fieldName: name

    }
}
export const selectSizeAction = (size) => {
    return ({
        type: ON_CHANGE_SELECT_SIZE,
        payload: size
    })
}
export const selectStatus = (values, name) => {
    return ({
        type: ON_CHANGE_SELECT_STATUS,
        value: values,
        fieldName: name
    })
}
export const closeModal = () => {
    return ({
        type: ON_CLICK_CLOSE_MODAL,
    })
}

export const getAllDrink = () => {
    return async (dispatch) => {
        try {
            let configDrink = {
                method: 'get',
                url: 'http://203.171.20.210:8080/devcamp-pizza365/drinks',

            }
            const reponseDrink = await axios(configDrink)
            const listDrink = await reponseDrink.data


            return dispatch({
                type: ALL_DRINK_AXIOS_SUCCESS,
                dataDrink: listDrink
            })

        } catch (error) {
            return dispatch({
                type: ALL_DRINK_AXIOS_ERROR,
                error: error
            })
        }
    }
}

