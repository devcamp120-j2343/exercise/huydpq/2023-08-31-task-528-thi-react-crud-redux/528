import { Pagination } from "@mui/material"
import { useSelector } from "react-redux"

const PaginationOrder = () => {
    return (
        <Pagination count={20} page={1}></Pagination>
    )
}

export default PaginationOrder