import { Typography } from "@mui/material"
import { useDispatch, useSelector } from "react-redux"
import { Button, Col, FormGroup, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap"
import { deteleOrder } from "../actinon/order.action"


const ModalDelete = ({ data, open, close }) => {
    const dispatch = useDispatch()
    
    const onBtnDeleteOrder = () => {
        dispatch(deteleOrder(data.id))
        toggle()
    }
    const toggle = () => {
        close();
    }
    return (
        <Modal
            isOpen={open}
            toggle={toggle}
        >
            <ModalHeader toggle={toggle}>Delete Order</ModalHeader>
            <ModalBody>
                <FormGroup row>
                    <Col sm={12}>
                        <Typography variant="body1">Bạn có chắc muốn xóa order {data.id}</Typography>
                    </Col>
                </FormGroup>

            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={onBtnDeleteOrder}>
                    Delete Order
                </Button>{' '}
                <Button color="secondary" onClick={toggle}>
                    Close
                </Button>
            </ModalFooter>
        </Modal>
    )
}

export default ModalDelete