

import { useDispatch, useSelector } from "react-redux"
import 'react-toastify/dist/ReactToastify.css';
import { Modal, ModalFooter, ModalHeader, ModalBody, Button, FormGroup, Label, Col, Input } from "reactstrap"
import { useEffect } from "react";
import { changeInput, closeModal, createOrder, getAllDrink, selectSizeAction } from "../actinon/order.action"
import { Valid, alert } from "../service/validate";
import { toast } from "react-toastify";
const CreateModal = ({ open, close }) => {

    const dispatch = useDispatch()
    const { modalValue, selectDrink } = useSelector(redux => redux.CreateOrderReducer)

    useEffect(() => {
        dispatch(getAllDrink())
    }, [])

    // Hàm change Select Pizza Size
    const onChangeSelectSize = (event, id) => {
        dispatch(selectSizeAction(event.target.value))
    }
    // Hàm change Select Loại Pizza
    const onChangeInput = (event, id) => {
        dispatch(changeInput(event.target.value, id))
    }
    // đóng modal
    const toggle = () => {
        close()
        dispatch(closeModal())
    }
    // hàm call API create order
    const onBtncreateOrder = () => {
        let check = Valid(modalValue)
        if (check) {
            dispatch(createOrder(modalValue))
            toggle()
        }

    }
    return (
        <Modal
            isOpen={open}
            toggle={toggle}
            size="lg"
        >
            <ModalHeader toggle={toggle}>Create Order</ModalHeader>
            <ModalBody>
                <FormGroup row>
                    <Label sm={2}>Size Pizza</Label>
                    <Col sm={10}>
                        < Input onChange={(event) => { onChangeSelectSize(event) }} type="select">
                            <option value="">--None--</option>
                            <option value="S">Small</option>
                            <option value="M">Medium</option>
                            <option value="L">Large</option>
                        </Input>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2}>Đường Kính</Label>
                    <Col sm={10}><Input disabled value={modalValue.duongKinh || ""}></Input></Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2}>Sườn</Label>
                    <Col sm={10}><Input disabled defaultValue={modalValue.suon || ""}></Input></Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2}>Salad</Label>
                    <Col sm={10}><Input disabled value={modalValue.salad || ""}></Input></Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2}>Số lượng nước</Label>
                    <Col sm={10}><Input disabled value={modalValue.soLuongNuoc || ""}></Input></Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2}>Thành tiền</Label>
                    <Col sm={10}><Input disabled value={modalValue.thanhTien || ""}></Input></Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2}>Loại pizza</Label>
                    <Col sm={10}>
                        < Input onChange={(event) => { onChangeInput(event, "loaiPizza") }} type="select">
                            <option value="">--None--</option>
                            <option value="Seafood">Hải sản</option>
                            <option value="Hawaii">Hawaii</option>
                            <option value="Bacon">Thịt hun khói</option>
                        </Input>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2}>Nước uống</Label>
                    <Col sm={10}>
                        < Input onChange={(event) => { onChangeInput(event, "idLoaiNuocUong") }} type="select">
                            <option value="">--None--</option>
                            {
                                selectDrink.map((element, index) => {
                                    return <option key={index} value={element.maNuocUong}>{element.tenNuocUong}</option>
                                })
                            }

                        </Input>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2}>Voucher</Label>
                    <Col sm={10}><Input onChange={(event) => { onChangeInput(event, "idVourcher") }} value={modalValue.idVourcher || ""}></Input></Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2}>Họ và tên</Label>
                    <Col sm={10}><Input onChange={(event) => { onChangeInput(event, "hoTen") }} value={modalValue.hoTen || ""}></Input></Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2}>Email</Label>
                    <Col sm={10}><Input onChange={(event) => { onChangeInput(event, "email") }} value={modalValue.email || ""}></Input></Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2}>Số điện thoại</Label>
                    <Col sm={10}><Input onChange={(event) => { onChangeInput(event, "soDienThoai") }} value={modalValue.soDienThoai || ""}></Input></Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2}>Địa chỉ</Label>
                    <Col sm={10}><Input onChange={(event) => { onChangeInput(event, "diaChi") }} value={modalValue.diaChi || ""}></Input></Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2}>Lời nhắn</Label>
                    <Col sm={10}><Input onChange={(event) => { onChangeInput(event, "loiNhan") }} value={modalValue.loiNhan || ""}></Input></Col>
                </FormGroup>
            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={onBtncreateOrder}>
                    Create Order
                </Button>{' '}
                <Button color="secondary" onClick={toggle}>
                    Close
                </Button>
            </ModalFooter>
        </Modal>

    )
}

export default CreateModal