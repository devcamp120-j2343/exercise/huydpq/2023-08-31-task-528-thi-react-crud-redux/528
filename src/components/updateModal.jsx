import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Modal, ModalFooter, ModalHeader, ModalBody, Button, FormGroup, Label, Col, Input } from "reactstrap"
import { closeModal, getOrderByID, selectStatus, updateOrder } from "../actinon/order.action";
import { toast } from "react-toastify";
import { alert } from "../service/validate";
const ModalUpdate = ({data ,open, close}) => {
    const [status, setStatus] = useState()
    const { modalValue, selectDrink } = useSelector(redux => redux.CreateOrderReducer)

    const dispatch = useDispatch()
    
    const onChangeStatus = (event, id) => {
        dispatch(selectStatus(event.target.value, id))
        setStatus({[id]: event.target.value})
    }
    
    useEffect (() => {
        dispatch(getOrderByID(data.orderCode))
    },[data]);

    const onBtnUpdateOrder = () => {
        dispatch(updateOrder(status, modalValue.id))
        toggle()
    }
    const toggle = () => {
        close();
        dispatch(closeModal())

    }
    return (
        <Modal
            isOpen={open}
            toggle={toggle}
            size="lg"
        >
            <ModalHeader toggle={toggle}>Update Order</ModalHeader>
            <ModalBody>
                <FormGroup row>
                    <Label sm={2}>Size Pizza</Label>
                    <Col sm={10}>
                        < Input value={modalValue.kichCo || ""} disabled type="select">
                            <option value="">--None--</option>
                            <option value="S">Small</option>
                            <option value="M">Medium</option>
                            <option value="L">Large</option>
                        </Input>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2}>Đường Kính</Label>
                    <Col sm={10}><Input disabled value={modalValue.duongKinh || ""}></Input></Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2}>Sườn</Label>
                    <Col sm={10}><Input disabled defaultValue={modalValue.suon || ""}></Input></Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2}>Salad</Label>
                    <Col sm={10}><Input disabled value={modalValue.salad || ""}></Input></Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2}>Số lượng nước</Label>
                    <Col sm={10}><Input disabled value={modalValue.soLuongNuoc || ""}></Input></Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2}>Thành tiền</Label>
                    <Col sm={10}><Input disabled value={modalValue.thanhTien || ""}></Input></Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2}>Loại pizza</Label>
                    <Col sm={10}>
                        < Input value={modalValue.loaiPizza || ""} disabled type="select">
                            <option value="">--None--</option>
                            <option value="Seafood">Hải sản</option>
                            <option value="Hawaii">Hawaii</option>
                            <option value="Bacon">Thịt hun khói</option>
                        </Input>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2}>Nước uống</Label>
                    <Col sm={10}>
                        < Input value={modalValue.idLoaiNuocUong || ""} disabled type="select">
                            <option value="">--None--</option>
                            {
                                selectDrink.map((element, index) => {
                                    return <option key={index} value={element.maNuocUong}>{element.tenNuocUong}</option>
                                })
                            }

                        </Input>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2}>Voucher</Label>
                    <Col sm={10}><Input disabled value={modalValue.idVourcher || ""}></Input></Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2}>Họ và tên</Label>
                    <Col sm={10}><Input disabled  value={modalValue.hoTen || ""}></Input></Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2}>Email</Label>
                    <Col sm={10}><Input disabled value={modalValue.email || ""}></Input></Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2}>Số điện thoại</Label>
                    <Col sm={10}><Input disabled value={modalValue.soDienThoai || ""}></Input></Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2}>Địa chỉ</Label>
                    <Col sm={10}><Input disabled value={modalValue.diaChi || ""}></Input></Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2}>Lời nhắn</Label>
                    <Col sm={10}><Input disabled value={modalValue.loiNhan || ""}></Input></Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2}>Trạng Thái</Label>
                    <Col sm={10}>
                        < Input  onChange={(event) => { onChangeStatus(event, "trangThai") }} type="select">
                            <option value="open">Open</option>
                            <option value="cancel">Đã Hủy</option>
                            <option value="confirmed">Đã xác nhận</option>
                        </Input>
                    </Col>
                </FormGroup>

            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={onBtnUpdateOrder}>
                    Update Order
                </Button>{' '}
                <Button color="secondary" onClick={toggle}>
                    Close
                </Button>
            </ModalFooter>
        </Modal>
    )
}

export default ModalUpdate