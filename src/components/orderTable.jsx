import { CircularProgress, Container, Grid, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from "@mui/material"
import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { fetchURL } from "../actinon/order.action"
import PaginationOrder from "./pagination"
import CreateModal from "./createModal"
import { Button } from "reactstrap"
import { ToastContainer } from "react-toastify"
import ModalUpdate from "./updateModal"
import ModalDelete from "./deleteModal"

const OrderTable = () => {
    const [openCreateModal, setOpenCreateModal] = useState(false)
    const [openModalUpdate, setOpenModalUpdate] = useState(false)
    const [openDeleteModal, setOpenDeleteModal] = useState(false)

    const [data, setData] = useState()
    const { listOrder, pending } = useSelector((redux) => redux.orderReducer)

    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(fetchURL())
    }, [])
    // hàm MỞ modal create
    const onBtnCreate = () => {
        setOpenCreateModal(true)
    }
    // hàm ĐÓNG modal create
    const closeModalCreate = () => {
        setOpenCreateModal(!openCreateModal)
    }
    // hàm nút Update
    const onBtnUpdateOrder = (userInfo) => {
        setData(userInfo)
        setOpenModalUpdate(true)
    }

    // hàm nút delete 
    const onBtnDeleteOrder = (usera) => {
        setOpenDeleteModal(true)
        setData(usera)
    }
    return (
        <div>

            <Container maxWidth="100%">

                <Grid container mt={2}>
                    <Grid item sm={12} md={12} lg={12} xs={12} textAlign={"center"}>
                        <Typography variant="h4">Quản lý đơn hàng </Typography>
                    </Grid>
                    <Grid item mt={2} sm={12} md={12} lg={12} xs={12}>
                        <Button onClick={onBtnCreate} color="success">Thêm mới</Button>
                    </Grid>
                    {
                        pending ?
                            <Grid item mt={5} sm={12} md={12} lg={12} xs={12} textAlign={"center"}>
                                <CircularProgress />
                            </Grid>
                            :
                            <Grid mt={2} item sm={12} md={12} lg={12} xs={12} >
                                <TableContainer component={Paper}>
                                    <Table sx={{ minWidth: 650 }} aria-label="a dense table">
                                        <TableHead>
                                            <TableRow sx={{ backgroundColor: "#8fb0ca" }}>
                                                <TableCell align="left">OrderCode</TableCell>
                                                <TableCell align="center">Kích Cỡ Combo</TableCell>
                                                <TableCell align="left">Loại Pizza</TableCell>
                                                <TableCell align="left">Nước Uống</TableCell>
                                                <TableCell align="left">Thành tiền</TableCell>
                                                <TableCell align="center">Họ và tên</TableCell>
                                                <TableCell align="left">Số điện thoại</TableCell>
                                                <TableCell align="left">Trạng Thái</TableCell>
                                                <TableCell align="center">Action</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {listOrder.map((user, index) => (
                                                <TableRow
                                                    key={index}
                                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                                >
                                                    <TableCell component="th" scope="row">{user.orderCode}</TableCell>
                                                    <TableCell align="center">{user.kichCo}</TableCell>
                                                    <TableCell align="left">{user.loaiPizza}</TableCell>
                                                    <TableCell align="left">{user.idLoaiNuocUong}</TableCell>
                                                    <TableCell align="left">{user.thanhTien}</TableCell>
                                                    <TableCell align="center">{user.hoTen}</TableCell>
                                                    <TableCell align="left">{user.soDienThoai}</TableCell>
                                                    <TableCell align="left">{user.trangThai}</TableCell>
                                                    <TableCell align="center"><Button onClick={() => onBtnUpdateOrder(user)} variant="contained" color="info">Update</Button>
                                                        <Button onClick={() => {onBtnDeleteOrder(user)}} variant="contained" color="danger">Delete</Button>
                                                    </TableCell>
                                                </TableRow>
                                            ))}
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Grid>
                    }

                    <Grid item mt={3} sm={12} md={12} lg={12} xs={12} display="flex" justifyContent={"center"}>
                        <PaginationOrder />
                    </Grid>

                </Grid>
            </Container>
            <CreateModal open={openCreateModal} close={closeModalCreate} />
            {
                data && <ModalUpdate data={data} open={openModalUpdate} close={() => { setOpenModalUpdate(!openModalUpdate) }} />
            }

            {
                data && <ModalDelete data={data} open={openDeleteModal} close={() => { setOpenDeleteModal(!openDeleteModal) }} />
            }
            <ToastContainer />
        </div>
    )
}

export default OrderTable