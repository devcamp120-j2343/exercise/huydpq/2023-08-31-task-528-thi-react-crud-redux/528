import { toast } from 'react-toastify';


export const alert = {
    position: "top-right",
    autoClose: 2000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
    theme: "light",
    }
 
export const Valid = (order) => {
    if(order.kichCo === "") {
        toast("Hãy chọn Size Pizza", alert)
        return false
    }
    if(order.loaiPizza === "") {
        toast("Hãy chọn Loại Pizza", alert)
        return false
    }
    if(order.idLoaiNuocUong === "") {
        toast("Hãy chọn nước uống", alert)
        return false
    }
    if(order.hoTen === "") {
        toast("Hãy nhập họ và tên", alert)
        return false
    }
    if(order.email === "") {
        toast("Hãy nhập email", alert)
        return false
    }
    if(order.soDienThoai === "") {
        toast("Hãy nhập số điện thoại", alert)
        return false
    }
    if(isNaN(order.soDienThoai)) {
        toast("Số điện thoại phải là số", alert)
        return false
    }
    if(order.diaChi === "") {
        toast("Hãy nhập địa chỉ", alert)
        return false
    }
    return true
}
