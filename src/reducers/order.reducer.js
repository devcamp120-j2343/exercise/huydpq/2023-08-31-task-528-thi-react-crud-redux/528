import { CREATE_ORDER_SUCCESS, DELETE_ORDER_ERROR, DELETE_ORDER_SUCCESS, UPDATE_ORDER_SUCCESS, USER_FETCH_ERROR, USER_FETCH_PENDING, USER_FETCH_SUCCESS } from "../constants/order.constant";

const initialState = {
    listOrder: [],
    pending: false,
}
const orderReducer = (state = initialState, action) => {
    switch (action.type) {
        case USER_FETCH_PENDING:
            state.pending = true
            break;
        case USER_FETCH_SUCCESS:
            state.listOrder = action.data
            state.pending = false
            break;
        case USER_FETCH_ERROR:
            break;
        case CREATE_ORDER_SUCCESS:
            state.listOrder = action.data
            break;
        case UPDATE_ORDER_SUCCESS:
            state.listOrder = action.data
            break;
        case DELETE_ORDER_SUCCESS:
            state.listOrder = action.data
            break;
        default:
            break;
    }
    return { ...state }
}

export default orderReducer