import { combineReducers } from "redux"
import orderReducer from "./order.reducer"
import { CreateOrderReducer } from "./createOrder.reducer" 
const rootReducers = combineReducers ({
    orderReducer,
    CreateOrderReducer
     
})

export default rootReducers
