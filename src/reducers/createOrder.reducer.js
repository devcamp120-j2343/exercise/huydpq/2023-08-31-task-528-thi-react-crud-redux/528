
import { ALL_DRINK_AXIOS_ERROR, ALL_DRINK_AXIOS_SUCCESS, CHANGE_INPUT, CREATE_ORDER_ERROR, CREATE_ORDER_SUCCESS, GET_ORDER_BY_ID_ERROR, GET_ORDER_BY_ID_SUCCESS, ON_CHANGE_SELECT_SIZE, ON_CHANGE_SELECT_STATUS, ON_CLICK_CLOSE_MODAL } from "../constants/order.constant";

const initialState = {
    modal: undefined,
    selectDrink: [],
    modalValue: {
        id: "",
        kichCo: "",
        duongKinh: "",
        suon: "",
        salad: "",
        soLuongNuoc: "",
        thanhTien: "",
        loaiPizza: "",
        idVourcher: "",
        idLoaiNuocUong: "",
        hoTen: "",
        email: "",
        soDienThoai: "",
        diaChi: "",
        loiNhan: "",
        trangThai: ""
    },
}


export const CreateOrderReducer = (state = initialState, action) => {
    switch (action.type) {
        case ON_CHANGE_SELECT_SIZE:
            if (action.payload === "") {
                state.modalValue.kichCo = "";
                state.modalValue.duongKinh = "";
                state.modalValue.suon = "";
                state.modalValue.salad = "";
                state.modalValue.soLuongNuoc = "";
                state.modalValue.thanhTien = "";
            } else if (action.payload === "S") {
                state.modalValue.kichCo = "S";
                state.modalValue.duongKinh = "20";
                state.modalValue.suon = "2";
                state.modalValue.salad = "200";
                state.modalValue.soLuongNuoc = "2";
                state.modalValue.thanhTien = "150000";
            } else if (action.payload === "M") {
                state.modalValue.kichCo = "M";
                state.modalValue.duongKinh = "25";
                state.modalValue.suon = "4";
                state.modalValue.salad = "300";
                state.modalValue.soLuongNuoc = "3";
                state.modalValue.thanhTien = "200000";
            } else if (action.payload === "L") {
                state.modalValue.kichCo = "L";
                state.modalValue.duongKinh = "30";
                state.modalValue.suon = "8";
                state.modalValue.salad = "500";
                state.modalValue.soLuongNuoc = "4";
                state.modalValue.thanhTien = "250000";
            }
            break;
        case ALL_DRINK_AXIOS_SUCCESS:
            state.selectDrink = action.dataDrink
            break;
        case ALL_DRINK_AXIOS_ERROR:
            break;
        case CHANGE_INPUT:
            let nameInp = action.fieldName
            state.modalValue[nameInp] = action.value
            break;
        case ON_CLICK_CLOSE_MODAL:
            for (let key in state.modalValue) {
                state.modalValue[key] = ""
            }
            break;
        case CREATE_ORDER_SUCCESS:
            state.modal = action.warning
            break;
        case CREATE_ORDER_ERROR:
            state.modal = action.warning
            break;
        case GET_ORDER_BY_ID_SUCCESS:
            for (let key in state.modalValue) {
                state.modalValue[key] = action.data[key]
            }
            break;
        case GET_ORDER_BY_ID_ERROR:
            state.modal = action.warning
            break;
        case ON_CHANGE_SELECT_STATUS:
            let name = action.fieldName
            state.modalValue[name] = action.value
            break;
        default:
            break;
    }
    return { ...state }
}