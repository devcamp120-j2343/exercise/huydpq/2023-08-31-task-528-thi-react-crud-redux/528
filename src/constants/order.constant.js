export const USER_FETCH_PENDING = "Trang thái đợi khi gọi API All order"

export const USER_FETCH_SUCCESS = "Trang thái thành công khi gọi API All order"

export const USER_FETCH_ERROR = "Trang thái lỗi khi gọi API All order"

export const CREATE_ORDER_PENDING = "Trang thái đợi khi gọi API create"

export const CREATE_ORDER_SUCCESS  = "Trang thái thành công khi gọi API create"

export const CREATE_ORDER_ERROR = "Trang thái lỗi khi gọi API create"

export const GET_ORDER_BY_ID_SUCCESS  = "Trang thái thành công khi gọi API get by ID"

export const GET_ORDER_BY_ID_ERROR = "Trang thái lỗi khi gọi API get by ID"

export const UPDATE_ORDER_SUCCESS  = "Trang thái thành công khi gọi API update"

export const UPDATE_ORDER_ERROR = "Trang thái lỗi khi gọi API update"

export const DELETE_ORDER_SUCCESS  = "Trang thái thành công khi gọi API delete"

export const DELETE_ORDER_ERROR = "Trang thái lỗi khi gọi API delete"

export const ALL_DRINK_AXIOS_SUCCESS = "Trang thái thành công khi gọi API drink"

export const ALL_DRINK_AXIOS_ERROR = "Trang thái lỗi khi gọi API drink"

export const ON_CHANGE_SELECT_SIZE = "Sự kiện change select size"
export const ON_CHANGE_SELECT_STATUS ="Sự kiện change select status"

export const CHANGE_INPUT = "Sự kiện Input"

export const ON_CLICK_CLOSE_MODAL= "Sự kiện đóng modal"

